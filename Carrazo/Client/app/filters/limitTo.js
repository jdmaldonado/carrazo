﻿CarrazoApp.filter('limitTo', function () {
    return function (input, end) {
        if (input) {
            //parse to int
            return input.slice(0, end);
        };
    };
});