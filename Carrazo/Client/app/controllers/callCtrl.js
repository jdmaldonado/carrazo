﻿CarrazoApp.controller('callCtrl', function callCtrl($scope, $rootScope, $routeParams, clients_Factory, $timeout) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.callSeconds = 0; /* Contador de Segundos */
    $scope.calledClient = true;
    $scope.messageCalledClient = false;
    $scope.showTime = false; /* Mensaje que muestra el tiempo de llamada*/
    $scope.noRegistredClient = false; /* Mensaje de Cliente no Registrado*/
    $scope.formClient = {}; /* Formulario Básico */
    $scope.clientes = {}; /* Select de Filtro */
    var countSystem; /* Sistema de conteo */

    /* Trae TODOS los clientes */
    var getClients = function () {
        $scope.clientes = clients_Factory.clients.GetClients({});
    }

    getClients();

    
    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.identification = undefined;
        $scope.formClient = {};
        getClients();
        $scope.calledClient = true;
        $scope.messageCalledClient = false;
        $scope.noRegistredClient = false;
        $scope.showTime = false;
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Load = function (identification) {
        $scope.identification = undefined;
        clearInterval(countSystem);
        $scope.messageCalledClient = false;
        $scope.showTime = false;
        $scope.formClient = null;
        $scope.noRegistredClient = false;

        $scope.callSeconds = 0; /* Reiniciamos el Tiempo */

        /*Traemos*/
        clients_Factory.client.GetByIdentification({ identification: identification }).$promise.then(function (data) {
            if (data.length > 0) {
                $scope.formClient = data[0];
            }

            /* Apenas consulta al cliente Empieza el contador hasta que que oprima el botón "llamar" */
            countSystem = setInterval(function () {
                $scope.callSeconds = $scope.callSeconds + 1000;
                $scope.$apply();
            }, 1000);

            /*Si el cliente Existe*/
            if ($scope.formClient != null) {

                /* Si el cliente Existe y no ha sido llamado*/
                if ($scope.formClient.estado == false) {
                    $scope.calledClient = false;
                    $scope.messageCalledClient = false;
                    $scope.showTime = true;
                }
                    /* Si el cliente Existe y ya ha sido llamado*/
                else {
                    $scope.calledClient = true;
                    $scope.messageCalledClient = true;
                    $scope.showTime = false;
                }
            }
                /* Si el cliente No Existe */
            else {
                $scope.calledClient = false;
                $scope.noRegistredClient = true;
            }

        });

        

    }

    /* Establece como "llamado" al cliente seleccionado */
    $scope.Call = function () {
        
        $scope.showTime = false;
        $scope.formClient.duracionLlamada = $scope.callSeconds;
        clients_Factory.callClient.call({ operatorCode: $scope.operatorCode }, $scope.formClient);
        clearInterval(countSystem);

        $scope.Clean();

    }

    
});

