﻿CarrazoApp.controller('typeCtrl', function typeCtrl($scope, $rootScope, $routeParams, clients_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.view = {
        editInfo: true,
        form: false
    };
        
    $scope.formClient = {}; /* Formulario Básico */ 
    

    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.identification = undefined;
        $scope.formClient = {};
        getClients();
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Update = function (identification) {

        /* Primero Esconda el formulario de edición y muestre el básico */
        $scope.view.editInfo = false;
        $scope.view.form = true;

        /*Traemos*/
        clients_Factory.client.GetByIdentification({ identification: identification }).$promise.then(function (data) {
            if (data.length > 0) {
                $scope.formClient = data[0];
            }

            /*Esté o no Registrado el cliente, cargue en el formulario la cedula que acaba de escribir*/
            $scope.formClient.cedula = identification;
            
        });
    }

    $scope.Save = function () {

        /* Si Existe un idCliente Quiere decir que se va a EDITAR, 
        de lo contrario voy a CREAR uno nuevo*/

        if ($scope.formClient.idCliente === undefined) {
            clients_Factory.createClient.PostClient({ operatorCode: $scope.operatorCode }, $scope.formClient);
        }
        else {
            clients_Factory.updateClient.putClient({ idClient: $scope.formClient.idCliente, operatorCode: $scope.operatorCode }, $scope.formClient);
        }

        $scope.Clean();

    }

    
});

