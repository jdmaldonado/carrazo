﻿CarrazoApp.controller('profileCtrl', function profileCtrl($scope, $rootScope, $routeParams, profiles_Factory, menus_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.view = {
        editInfo: false,
        form: true
    };
        
    $scope.formProfile = {}; /* Formulario Básico */
    $scope.perfiles = {}; /* Select de Filtro */

    /* Trae TODOS los clientes */
    var getProfiles = function () {
        $scope.perfiles = profiles_Factory.profiles.GetProfiles({});
    }

    getProfiles();

    /* Muestra el Formulario de edición y oculta el formulario Principal*/
    $scope.loadEditForm = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.profile = undefined;
    }

    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.view.editInfo = false;
        $scope.view.form = true;
        $scope.profile = undefined;
        $scope.formProfile = {};
        getProfiles();
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Update = function (profileName) {

        /* Primero Esconda el formulario de edición y muestre el básico */
        $scope.view.editInfo = false;
        $scope.view.form = true;

        /*como solo recibimos la cédula hay que recorrer el listado de clientes y traer el que coincida, con esto llenamos el formulario base*/
        $scope.perfiles.map(function (objeto) {
            if ( objeto.perfil == profileName) {
                $scope.formProfile = objeto;
            }
        })

    }

    /* Traiga los Roles y diga si están asociados actualmente o no al perfil */
    $scope.ManageRoles = function (profileName) {

        var obj_profile = {};
        $scope.perfiles.map(function (objeto) {
            if (objeto.perfil == profileName) {
                obj_profile = objeto;
            }
        })

        $scope.roles = menus_Factory.roles.GetRolesByIdPerfil({ idPerfil: obj_profile.idPerfil });
    }

    /* Agregue o Elimine Roles a el perfil*/
    $scope.AddOrDeleteRol = function (obj_rol) {

        if (obj_rol.activo == false) {

            menus_Factory.deleteRol.DeleteRolToPerfil({ idPerfil: obj_rol.idPerfil, idRol: obj_rol.idRol }).$promise.then(function () {
                $scope.ManageRoles(obj_rol.perfil);
            });
        }
        else {
            menus_Factory.addRol.AddRolToPerfil({ idPerfil: obj_rol.idPerfil, idRol: obj_rol.idRol }).$promise.then(function () {
                $scope.ManageRoles(obj_rol.perfil);
            });
        }
        
    }

    /*Guarda o edita el Perfil*/
    $scope.Save = function () {

        /* Si Existe un idCliente Quiere decir que se va a EDITAR, 
        de lo contrario voy a CREAR uno nuevo*/

        if ($scope.formProfile.idCliente === undefined) {
            profiles_Factory.createProfile.postProfile({}, $scope.formProfile).$promise.then(function () {
                $scope.Clean();
            });
        }
        else {
            profiles_Factory.updateProfile.putProfile({ idPerfil: $scope.formProfile.idPerfil }, $scope.formProfile).$promise.then(function () {
                $scope.Clean();
            });
        }
    }

    
});

