﻿CarrazoApp.controller('clientCtrl', function clientCtrl($scope, $rootScope, $filter, $routeParams, clients_Factory, downloadSpreadsheet_factory) {

    $scope.filter =
    {
        status: null,
        startDate: null,
        finalDate: null
    }

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    /* Pagination Variables */
        $scope.maxSize = 5; /* Cantidad de Botones que se muestran de la paginacion */
        $scope.currentPage = 1;/* Página Actual */
        $scope.pageSize = 10; /* Cantidad de Items por cada Página + 1*/
    /**/

    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.enableDownload = false;
    $scope.showEmptyMessage = false;


    $scope.requestData = function () {

        $scope.currentPage = 1;

        $scope.filter.startTypeDate = $filter('unixTime_Filter')($scope.startType);
        $scope.filter.finalTypeDate = $filter('unixTime_Filter')($scope.endType);
        $scope.filter.startCallDate = $filter('unixTime_Filter')($scope.startCall);
        $scope.filter.finalCallDate = $filter('unixTime_Filter')($scope.endCall);

        clients_Factory.clientsGrid.query($scope.filter).$promise.then(function (data) {
           
            if (data.length < 1) {
                $scope.showEmptyMessage = true;
            }
            else {
                $scope.totalItems = data.length;
                $scope.clientes = data;
                $scope.enableDownload = true;
                $scope.showEmptyMessage = false;
            }
            
        });
    }

    $scope.exportData = function () {

        $scope.filter.format = 'spreadsheet';

        downloadSpreadsheet_factory.downloadIt($scope.filter, "Clients/Historical");

    }


    /* DATEPICKER  */
    $scope.todayStartTypeDate = function () {
        $scope.startType = new Date();
    };

    $scope.todayFinalTypeDate = function () {
        $scope.endType = new Date();
    };

    $scope.cleanStartTypeDate = function () {
        $scope.startType = null;
    };

    $scope.cleanEndTypeDate = function () {
        $scope.endType = null;
    };

    $scope.todayStartCallDate = function () {
        $scope.startCall = new Date();
    };

    $scope.todayFinalCallDate = function () {
        $scope.endCall = new Date();
    };

    $scope.cleanStartCallDate = function () {
        $scope.startCall = null;
    };

    $scope.cleanEndCallDate = function () {
        $scope.endCall = null;
    };

    $scope.cleanStartTypeDate();
    $scope.cleanEndTypeDate();
    $scope.cleanStartCallDate();
    $scope.cleanEndCallDate();

    $scope.openStartTypeDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.startTypeDateOpened = true;
    };

    $scope.openFinalTypeDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.finalTypeDateOpened = true;
    };

    $scope.openStartCallDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.startCallDateOpened = true;
    };

    $scope.openFinalCallDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.finalCallDateOpened = true;
    };

    $scope.format = 'dd/MM/yyyy';


});