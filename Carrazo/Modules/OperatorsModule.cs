﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Carrazo.DataBase;
using Carrazo.Models;
using Carrazo.Services;

namespace Carrazo.Modules
{
    public class OperatorsModule : NancyModule
    {
        public OperatorsModule()
            : base("Operators")
        {
            Get["/"] = parameters =>
            {
                var _operators = DbManager.GetOperators();

                return _operators;
            };

            Put["/update/{idOperador}"] = parameters =>
            {
                var _operator = this.Bind<Operator>();

                var editedOperator = DbManager.UpdateOperator(_operator);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/create"] = parameters =>
            {
                var _operator = this.Bind<Operator>();

                Operator createdOperator = DbManager.CreateOperator(_operator);

                return createdOperator;
            };
        }
    }
}