﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Carrazo.DataBase;
using Carrazo.Models;
using Carrazo.Services;

namespace Carrazo.Modules
{
    public class ProfilesModule : NancyModule
    {
        public ProfilesModule()
            : base("Profiles")
        {
            Get["/"] = parameters =>
            {
                var profiles = DbManager.GetProfiles();

                return profiles;
            };

            Put["/update/{idPerfil}"] = parameters =>
            {
                var profile = this.Bind<Profile>();

                var editedProfile = DbManager.UpdateProfile(profile);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/create"] = parameters =>
            {
                var perfil = this.Bind<Profile>();

                Profile createdProfile = DbManager.CreateProfile(perfil);

                return createdProfile;
            };
        }
    }
}