﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.Security;

namespace Carrazo.Modules
{
    public class LoginModule : NancyModule
    {        
        public LoginModule()
        {
            this.RequiresAuthentication();

            Get["/"] = parameters =>
            {
                return new Response { StatusCode = HttpStatusCode.OK };
            };
        }
    }
}