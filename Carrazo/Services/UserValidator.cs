﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy.Authentication.Basic;
using Nancy.Security;
using Carrazo.DataBase;

namespace Carrazo.Services
{
    public class UserValidator : IUserValidator
    {
        public IUserIdentity Validate( string userName, string password)
        {
            var currentOperator = DbManager.ValidateOperator(userName);

            if (currentOperator != null)
            {
                return new IUserIdentity { UserName = userName };
            }
            else
            {
                return null;
            }
        }
    }

    public class UserIdentity : IUserIdentity
    {
        public string UserName { get; set; }
        public IEnumerable<string> Claims { get; set; }
    }
}