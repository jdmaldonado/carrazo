﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Carrazo.Models;
using Carrazo.DataBase;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace Carrazo.Services
{
    public class ClientService
    {
        public Client SetOperatorIdForCreation(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.IdDigitador = _operator.IdOperador;

            Client createdClient = DbManager.CreateClient(client);

            return createdClient;
        }

        public dynamic SetOperatorIdForUpdate(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.ModificadoPor = _operator.IdOperador;
            client.FechaModificacion = DateTime.Now;

            var updateClient = DbManager.UpdateClient(client);

            return updateClient;
        }

        public dynamic SetOperatorIdForCall(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.LlamadoPor = _operator.IdOperador;
            client.FechaLlamada = DateTime.Now;
            client.Estado = true; //true: Llamado, false: sin Llamar

            var calledClient = DbManager.SetClientToCalled(client);

            return calledClient;
        }

        public dynamic SetOperatorIdForCallStatus(Client client, long operatorCode)
        {
            Operator _operator = DbManager.GetOperatorFromCode(operatorCode);

            client.ModificadoPor = _operator.IdOperador;

            var calledClient = DbManager.ChangeCallStatus(client);

            return calledClient;
        }

        public dynamic SetOperatorNameForGet()
        {

            List<Client> _clients = DbManager.GetClients();

            foreach (var _client in _clients)
            {

                Operator _operator = _client.LlamadoPor == null ? null : DbManager.GetOperatorFromId(_client.LlamadoPor.Value);

                if (_operator == null)
                {
                    continue;
                }
                else
                {
                    _client.Asesor = _operator.Nombres + " " + _operator.Apellidos;
                }

            }

            return _clients;
        }

        public List<Client> SetNamesForGrid(List<Client> clients)
        {

            foreach (var _client in clients)
            {
                Operator _operatorAsesor = _client.LlamadoPor == null ? null : DbManager.GetOperatorFromId(_client.LlamadoPor.Value);
                Operator _operatorDigitador = DbManager.GetOperatorFromId(_client.IdDigitador);
                Operator _operatorModificador = _client.ModificadoPor == null ? null : DbManager.GetOperatorFromId(_client.ModificadoPor.Value);


                /*Asesor*/
                _client.Asesor = _operatorAsesor == null ? null : _operatorAsesor.Nombres + " " + _operatorAsesor.Apellidos;


                /*Digitador*/
                _client.Digitador = _operatorDigitador == null ? null : _operatorDigitador.Nombres + " " + _operatorDigitador.Apellidos;


                /*Modificador*/
                _client.Modificador = _operatorModificador == null ? null : _operatorModificador.Nombres + " " + _operatorModificador.Apellidos;

            }

            return clients;
        }

        public Stream GenerateExcelFileForClients(List<Client> records, CultureInfo culture)
        {

            var headers = new List<object[]>();
            headers.Add(GetSpreadSheetHeader());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Historical Clients");


            var recordsDecoded = new List<object[]>();


            foreach (var record in records)
                recordsDecoded.Add(ConvertClientToObject(record, culture));


            ws.Cells["A1"].LoadFromArrays(headers);

            using (ExcelRange rng = ws.Cells["A1:AW1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                rng.Style.Font.Color.SetColor(Color.White);
            }


            if (recordsDecoded.Count > 0)
            {

                ws.Cells["A2"].LoadFromArrays(recordsDecoded);

                using (ExcelRange col = ws.Cells[2, 40, 2 + records.Count(), 41])
                {
                    col.Style.Numberformat.Format = "0.00%";
                }

                using (ExcelRange col = ws.Cells[2, 44, 2 + records.Count(), 44])
                {
                    col.Style.Numberformat.Format = "0.00%";
                }
            }


            return new MemoryStream(pck.GetAsByteArray());
        }

        private object[] GetSpreadSheetHeader()
        {
            return new object[] {
                                    "IdCliente", 
                                    "Cedula",
                                    "Nombres", 
                                    "Apellidos",
                                    "Celular",
                                    "Teléfono",
                                    "Correo",
                                    "Estado",
                                    "Estado Llamada",
                                    "Asesor Llamada",
                                    "Digitador",
                                    "Modificador Por",
                                    "Fecha Creación",
                                    "Fecha Modificación",
                                    "Codigo Encuesta",
                                    "Fecha LLamada",
                                    "Duracion Llamada (segundos)"
            };
        }

        private object[] ConvertClientToObject(Client client, CultureInfo culture)
        {
            string status = client.Estado == true ? "Llamado" : "Sin Llamar";
            string statusCall = GetCallStatusString(client.EstadoLlamada);


            return new object[] { client.IdCliente.ToString(),
                                    client.Cedula,
                                    client.Nombres,
                                    client.Apellidos,
                                    client.Celular,
                                    client.Telefono,
                                    client.Correo,
                                    status,
                                    statusCall,
                                    client.Asesor,
                                    client.Digitador,
                                    client.Modificador,
                                    client.FechaCreacion.ToString(culture),
                                    client.FechaModificacion != null ? client.FechaModificacion.Value.ToString(culture) : string.Empty,
                                    client.CodigoEncuesta,
                                    client.FechaLlamada != null ? client.FechaLlamada.Value.ToString(culture) : string.Empty,
                                    (client.DuracionLlamada/1000).ToString()
            };
        }

        private string GetCallStatusString(short? idCallStatus)
        {
            if (idCallStatus != null)
            {
                switch (idCallStatus)
                {
                    case 0: return "Existosa";
                    case 1: return "No Existosa";
                    case 2: return "No Contactar";
                    default: return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }            
        }
    }

}