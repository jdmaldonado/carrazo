﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Carrazo.Models;

namespace Carrazo.DataBase
{
    public class DbManager
    {
        /*POST*/
        public static dynamic AddRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.IdPerfil = idPerfil;
            record.IdRol = idRol;

            var addedRol = db.PerfilesRoles.Insert(record);


            return addedRol;
        }

        public static dynamic CreateClient(Client client)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.Cedula = client.Cedula;
            record.Nombres = client.Nombres;
            record.Apellidos = client.Apellidos;
            record.Celular = client.Celular;
            record.Telefono = client.Telefono;
            record.Estado = client.Estado;
            record.LlamadoPor = client.LlamadoPor;
            record.IdDigitador = client.IdDigitador;
            record.ModificadoPor = client.ModificadoPor;
            record.CodigoEncuesta = client.CodigoEncuesta;
            record.Correo = client.Correo;

            var createdClient = db.Clientes.Insert(record);


            return createdClient;
        }

        public static dynamic CreateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.Codigo = _operator.Codigo;
            record.Nombres = _operator.Nombres;
            record.Apellidos = _operator.Apellidos;
            record.IdPerfil = _operator.IdPerfil;

            var createdOperator = db.Operadores.Insert(record);


            return createdOperator;
        }

        public static dynamic CreateProfile(Profile profile)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.Perfil = profile.Perfil;

            var editedProfile = db.Perfiles.Insert(record);


            return editedProfile;
        }


        /*GET*/
        public static Client GetCallStatusByClientId(long identification)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            Client client = db.Clientes.All()
                .Select(
                    db.Clientes.IdCliente,
                    db.Clientes.EstadoLlamada.As("EstadoActualLlamada"),
                    db.Clientes.Estado
                )
                .Where(db.Clientes.Cedula == identification).FirstOrDefault();

            return client;
        }

        public static Client GetClientByIdentification(long identification)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            Client client = db.Clientes.All()
                .Where(db.Clientes.Cedula == identification).FirstOrDefault();

            return client;
        }
        
        public static List<Client> GetClients()
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            List<Client> clients = db.Clientes.All();

            return clients;
        }

        public static List<Client> GetClientsForGrid(Boolean? status, DateTime? initialTypeDate, DateTime? finalTypeDate, DateTime? initialCallDate, DateTime? finalCallDate)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            Boolean currentStatus = false;
            string _initialTypeDate, _finalTypeDate, _initialCallDate, _finalCallDate;

            _initialTypeDate = initialTypeDate != null ? initialTypeDate.Value.ToString("yyyy-MM-dd 00:00:00") : null;
            _finalTypeDate = finalTypeDate != null ? finalTypeDate.Value.ToString("yyyy-MM-dd 23:59:59") : null;
            _initialCallDate = initialCallDate != null ? initialCallDate.Value.ToString("yyyy-MM-dd 00:00:00") : null;
            _finalCallDate = finalCallDate != null ? finalCallDate.Value.ToString("yyyy-MM-dd 23:59:59") : null;


            List<dynamic> expressions = new List<dynamic>();

            if (status != null)
            {
                currentStatus = status.Value;
                expressions.Add(db.Clientes.Estado == currentStatus);
            }

            if (_initialTypeDate != null)
                expressions.Add(db.Clientes.FechaCreacion >= _initialTypeDate);

            if (_finalTypeDate != null)
                expressions.Add(db.Clientes.FechaCreacion <= _finalTypeDate);

            if (_initialCallDate != null)
                expressions.Add(db.Clientes.FechaLLamada >= _initialCallDate);

            if (_finalCallDate != null)
                expressions.Add(db.Clientes.FechaLLamada <= _finalCallDate);


            dynamic completeQuery = db.Clientes.FechaCreacion <= DateTime.Now;

            foreach (var expression in expressions)
            {
                completeQuery = completeQuery && expression;
            }

            List<Client> clients = db.Clientes.All().Where(completeQuery);

            return clients;

        }

        public static List<Operator> GetOperators()
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            List<Operator> operators = db.Operadores.All();

            return operators;
        }

        public static Operator GetOperatorFromCode(long operatorCode)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            Operator _operator = db.Operadores.All()
           .Where(db.Operadores.Codigo == operatorCode).FirstOrDefault();

            return _operator;
        }

        public static Operator GetOperatorFromId(int? idOperator)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            Operator _operator = db.Operadores.All()
           .Where(db.Operadores.IdOperador == idOperator).FirstOrDefault();

            return _operator;
        }

        public static List<Menu> GetMenus(long operatorCode)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

             List<Menu> menus = db.Operadores
            .Select(
                db.Operadores.IdOperador,
                db.Operadores.Nombres,
                db.Operadores.Codigo,
                db.Roles.Rol,
                db.Roles.Icono,
                db.Roles.Href
            )
            .Join(db.PerfilesRoles).On(db.Operadores.IdPerfil == db.PerfilesRoles.IdPerfil)
            .Join(db.Roles).On(db.PerfilesRoles.IdRol == db.Roles.IdRol)
            .Where(db.Operadores.Codigo == operatorCode)
            .OrderBy(db.Roles.Rol);


            return menus;
        }

        public static List<Profile> GetProfiles()
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            List<Profile> profiles = db.Perfiles.All();

            return profiles;
        }

        public static List<Role> GetRolesByIdProfile(int idPerfil)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            List<Role> roles = db.sp_GetRolesByIdProfile(idPerfil);

            return roles;
        }


        /*PUT*/
        public static dynamic ChangeCallStatus(Client client)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.IdCliente = client.IdCliente;
            record.ModificadoPor = client.ModificadoPor;
            record.EstadoLlamada = client.EstadoLlamada;

            var updateClient = db.Clientes.UpdateByIdCliente(record);

            return updateClient;
        }
        
        public static dynamic SetClientToCalled(Client client)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.IdCliente = client.IdCliente;
            record.LlamadoPor = client.LlamadoPor;
            record.FechaLlamada = client.FechaLlamada;
            record.Estado = client.Estado;
            record.DuracionLlamada = client.DuracionLlamada;
            record.EstadoLlamada = client.EstadoLlamada;

            var updateClient = db.Clientes.UpdateByIdCliente(record);

            return updateClient;
        }

        public static dynamic UpdateClient(Client client)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.IdCliente = client.IdCliente;
            record.Cedula = client.Cedula;
            record.Nombres = client.Nombres;
            record.Apellidos = client.Apellidos;
            record.Celular = client.Celular;
            record.Telefono = client.Telefono;
            record.ModificadoPor = client.ModificadoPor;
            record.FechaModificacion = client.FechaModificacion;
            record.CodigoEncuesta = client.CodigoEncuesta;
            record.Correo = client.Correo;

            var updateClient = db.Clientes.UpdateByIdCliente(record);

            return updateClient;
        }

        public static dynamic UpdateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            dynamic record = new SimpleRecord();
            record.IdOperador = _operator.IdOperador;
            record.Codigo = _operator.Codigo;
            record.Nombres = _operator.Nombres;
            record.Apellidos = _operator.Apellidos;
            record.IdPerfil = _operator.IdPerfil;

            var updateOperator = db.Operadores.UpdateByIdOperador(record);

            return updateOperator;
        }

        public static int UpdateProfile( Profile profile)
        {
            var db = Database.OpenNamedConnection("CARRAZO");


            dynamic record = new SimpleRecord();
            record.IdPerfil = profile.IdPerfil;
            record.Perfil = profile.Perfil;
            

            int editedProfile = db.Perfiles.UpdateByIdPerfil(record);


            return editedProfile;
        }

        
        /*DELETE*/
        public static dynamic QuitRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("CARRAZO");

            var deletedRol = db.PerfilesRoles.Delete(idPerfil: idPerfil, idRol: idRol);

            return deletedRol;
        }
       
    }
}