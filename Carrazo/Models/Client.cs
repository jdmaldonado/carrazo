﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carrazo.Models
{
    public class Client
    {
        public int IdCliente { get; set; }
        public long Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Celular { get; set; }
        public string Telefono { get; set; }
        public Boolean Estado { get; set; }
        public short? EstadoActualLlamada { get; set; }
        public short? EstadoLlamada { get; set; }
        public int? LlamadoPor { get; set; }
        public string Asesor { get; set; }
        public int IdDigitador { get; set; }
        public string Digitador { get; set; }
        public int? ModificadoPor { get; set; }
        public string Modificador { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public string CodigoEncuesta { get; set; }
        public string Correo { get; set; }
        public DateTime? FechaLlamada { get; set; }
        public long DuracionLlamada { get; set; }
    }
}